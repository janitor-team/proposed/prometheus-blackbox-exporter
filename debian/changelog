prometheus-blackbox-exporter (0.19.0-3) unstable; urgency=medium

  * Add patch to replace go-kit/kit/log with go-kit/log (Closes: #1010054)
  * Bump Standards-Version to 4.6.1 (no changes)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sat, 14 May 2022 15:34:39 +0000

prometheus-blackbox-exporter (0.19.0-2) unstable; urgency=medium

  * Team upload.
  * Update default file for new upstream release.
  * Install NOTICE file for Apache-2.0 license compliance.

 -- Guillem Jover <gjover@sipwise.com>  Tue, 15 Mar 2022 23:05:26 +0100

prometheus-blackbox-exporter (0.19.0-1) unstable; urgency=medium

  * Team upload.
  * Update gitlab-ci.yml from its new refactored template.
  * Update gbp.conf following latest Go Team workflow.
  * Update debian/watch file.
  * Use dh-sequence-golang instead of dh-golang and --with=golang.
  * Switch to Standards-Version 4.6.0 (no changes needed).
  * Add Repository-Browse upstream metadata field.
  * Update Catalan debconf translation.
  * Remove Files-Excluded from debian/copyright.
  * Refresh and remove patches for new upstream release.
  * Update dependencies for new upstream release.

 -- Guillem Jover <gjover@sipwise.com>  Tue, 30 Nov 2021 23:21:56 +0100

prometheus-blackbox-exporter (0.18.0+ds-3) unstable; urgency=medium

  * Team upload.

  [ Américo Monteiro ]
  * Update Portuguese debconf translation (Closes: #982331)

  [ Adriano Rafael Gomes ]
  * Update Brazilian Portuguese debconf translation (Closes: #987435)

  [ Camaleón ]
  * Update Spanish debconf translation (Closes: #987596)

  [ Guillem Jover ]
  * Add Catalan debconf translation

 -- Guillem Jover <gjover@sipwise.com>  Fri, 28 May 2021 01:31:50 +0200

prometheus-blackbox-exporter (0.18.0+ds-2) unstable; urgency=medium

  * Team upload.

  [ Daniel Swarbrick ]
  * Bump Standards-Version to 4.5.1 (no changes).
  * d/po/de.po: remove obsolete FIXME comment

  [ Frans Spiesschaert ]
  * Add initial Dutch debconf translation (Closes: #969342).

  [ Jean-Pierre Giraud ]
  * Add initial French debconf translation (Closes: #969480).

  [ Helge Kreutzmann ]
  * Minor grammar fix in debian/templates (Closes: #968857).
  * Add initial German debconf translation (Closes: #968856).

  [ Guillem Jover ]
  * Switch to debian/watch format version 4
  * Indent debian/watch opts contents
  * Decapitalize synopsis first word
  * Move note about license location on Debian systems into a Comment field
  * Update gitlab-ci.yml from latest upstream version
  * Use $(BUILDDIR) uniformly instead of the literal build/
  * Update version.BuildUser to match the Maintainer address
  * Remove nocheck handling from override_dh_auto_test
  * Change systemd service Restart directive from always to on-failure
  * Add man:prometheus-blackbox-exporter(1) to systemd unit Documentation field
  * Use '' instead of `' in text
  * Use $() instead of `` in init script
  * Remove $syslog dependency from init script
  * Switch from /var/run to /run
  * Add missing dependency on adduser
  * Rewrite init script using start-stop-daemon
  * Unify log files removal during purge into a single entry
  * Add missing CFGFILE variable to fix init script config check
  * Remove error suppression from postinst
  * Run adduser unconditionally
  * Do not change pathname metadata if there are dpkg statoverrides in place
  * Update copyright claims
  * Update gitignore entries

 -- Guillem Jover <gjover@sipwise.com>  Fri, 29 Jan 2021 00:27:44 +0100

prometheus-blackbox-exporter (0.18.0+ds-1) unstable; urgency=medium

  * New upstream release.
  * Add 02-Enable_HTTP2.patch to facilitate building with newer
    golang-github-prometheus-common-dev package.
  * Revert unwarranted renaming of systemd service file.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Tue, 20 Oct 2020 14:51:09 +0000

prometheus-blackbox-exporter (0.17.0+ds-1) unstable; urgency=medium

  [ Daniel Swarbrick ]
  * New upstream version.
  * Set upstream metadata fields: Repository.
  * Bump Standards-Version to 4.5.0 (rename service / init files).
  * Bump debhelper-compat version to 13.
  * Drop obsolete 02-yaml-v2.patch.
  * Drop obsolete 99-Skip_network_test.patch.
  * Update yaml build-dep package name.
  * d/control: add Rules-Requires-Root.
  * d/templates: mark description as translatable.
  * Run debconf-updatepo.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ Martina Ferrari ]
  * Update my email.
  * Update team email address in Maintainer.
  * Update debian/defaults.
  * debian/control: run wrap-and-sort, and fix Section.
  * debian/dirs: Create shared directories.
  * debian/examples: Add missing example.yml.
  * debian/init: Add SysV init support.
  * debian/rules, debian/install, debian/manpages: Generate manpage and unify
    package build with other exporters.
  * debian/logrotate: Add logrotate configuration.
  * debian/watch: Consider upstream RC version naming.

 -- Martina Ferrari <tina@debian.org>  Mon, 03 Aug 2020 22:17:02 +0000

prometheus-blackbox-exporter (0.16.0+ds-1) unstable; urgency=medium

  [ Daniel Swarbrick ]
  * New upstream release.
  * Drop unnecessary 02-Client_API_change.patch.
  * Drop unnecessary 03-promlog-fix.patch.
  * Add patch to revert upstream yaml.v3 dependency.
  * Update Standards-Version to 4.4.1 (debhelper-compat = 12).
  * Bump required Go version to 1.13.
  * Add myself to uploaders.
  * Skip additional network-dependent test, and only run short tests.

  [ Martina Ferrari ]
  * Update my name.
  * Improve init script.
  * Add Pre-Depends as requested by lintian.

 -- Martina Ferrari <tina@debian.org>  Fri, 03 Jan 2020 15:53:54 +0000

prometheus-blackbox-exporter (0.13.0+ds-2) unstable; urgency=medium

  * Ask the user to have CAP_NET_RAW enabled for blackbox-exporter
    (Closes: #872997)

 -- Filippo Giunchedi <filippo@debian.org>  Sat, 19 Jan 2019 15:59:39 +0100

prometheus-blackbox-exporter (0.13.0+ds-1) unstable; urgency=medium

  [ Filippo Giunchedi ]
  * New upstream release.
  * debian/patches/03-promlog-fix.patch:
      update promlog invocation for newer golang-github-prometheus-common-dev

  [ Martina Ferrari ]
  * debian/control: Shorten version matching in depends to satisfy lintian
    and ease backports.
  * Update Standards-Version with no changes.

 -- Martina Ferrari <tina@debian.org>  Wed, 26 Dec 2018 17:01:09 +0000

prometheus-blackbox-exporter (0.12.0+ds-2) unstable; urgency=high

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Martina Ferrari ]
  * Backport 9d8ad26 to fix client API change. Closes: #906503.
    Raising urgency for RC bug fix.
  * debian/gbp.conf: Stop using pristine tar, and update branch names.
  * debian/control: Update Standards-Version (no changes).

 -- Martina Ferrari <tina@debian.org>  Sun, 19 Aug 2018 03:07:32 +0000

prometheus-blackbox-exporter (0.12.0+ds-1) unstable; urgency=medium

  * New upstream release. Closes: #894130
  * debian/control: Update dependency version.
  * debian/copyright: Add Files-Excluded header.
  * debian/{postinst,init}: Use non-recursive chown.
  * Update debhelper compat to 11.

 -- Martina Ferrari <tina@debian.org>  Mon, 26 Mar 2018 22:14:14 +0000

prometheus-blackbox-exporter (0.11.0+ds-4) unstable; urgency=high

  * Disable completely tcp_test.go until upstream fixes the tests.
    Closes: 887070

 -- Martina Ferrari <tina@debian.org>  Sat, 13 Jan 2018 13:41:14 +0000

prometheus-blackbox-exporter (0.11.0+ds-3) unstable; urgency=high

  * Disable a race-prone test that is causing FTBFS in many architectures
    (raise urgency).
  * Update Standards-Version.

 -- Martina Ferrari <tina@debian.org>  Tue, 09 Jan 2018 17:41:35 +0000

prometheus-blackbox-exporter (0.11.0+ds-2) unstable; urgency=medium

  * Add missing documentation.

 -- Martina Ferrari <tina@debian.org>  Thu, 14 Dec 2017 19:52:28 +0000

prometheus-blackbox-exporter (0.11.0+ds-1) unstable; urgency=medium

  * Replace dpkg-parsechangelog with /usr/share/dpkg/pkg-info.mk
  * New upstream release.
  * Update dependencies.
  * Automated cme updates.
  * Update fixtures location.
  * Update defaults.
  * Fix watchfile.

 -- Martina Ferrari <tina@debian.org>  Thu, 14 Dec 2017 19:42:09 +0000

prometheus-blackbox-exporter (0.7.0+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Add watchfile.
  * debian/control:
    - Add myself to Uploaders.
    - Update Standards-Version (no changes).
    - Replace golang-go with golang-any in Build-Depends.
    - Mark package as autopkgtest-able.
    - Refresh dependencies.
  * Replace debian/rules trick for creating tarballs with gbp config.

 -- Martina Ferrari <tina@debian.org>  Fri, 14 Jul 2017 18:28:23 +0000

prometheus-blackbox-exporter (0.3.0+ds1-1) unstable; urgency=medium

  * Initial release (Closes: #843189)

 -- Filippo Giunchedi <filippo@debian.org>  Mon, 7 Nov 2016 00:25:52 +0000
