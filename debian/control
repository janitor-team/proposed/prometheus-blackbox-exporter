Source: prometheus-blackbox-exporter
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Filippo Giunchedi <filippo@debian.org>,
           Martina Ferrari <tina@debian.org>,
           Daniel Swarbrick <dswarbrick@debian.org>,
Section: net
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any (>= 2:1.13~),
               golang-github-andybalholm-brotli-dev,
               golang-github-go-kit-log-dev,
               golang-github-miekg-dns-dev (>= 1),
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev (>= 0.14.0~),
               golang-github-prometheus-exporter-toolkit-dev,
               golang-golang-x-net-dev,
               golang-gopkg-alecthomas-kingpin.v2-dev,
               golang-gopkg-yaml.v3-dev,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-blackbox-exporter
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-blackbox-exporter.git
Homepage: https://github.com/prometheus/blackbox_exporter
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/prometheus/blackbox_exporter

Package: prometheus-blackbox-exporter
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: adduser,
         debconf,
         libcap2-bin,
         ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: blackbox prober for Prometheus
 The blackbox exporter allows blackbox probing of network endpoints over HTTP,
 HTTPS, DNS, TCP and ICMP. Additional modules can be defined to suit other
 needs.
 .
 Querying of endpoints happens via HTTP GET queries, by specifying the target
 name and what kind of probing to execute. Results from the probe are returned
 as a set of Prometheus metrics.
